// 14/1/2019

const express =  require('express')
const app = express()
const _ = require('lodash')
const bodyParser = require('body-parser')

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
   extended: false
}))

// parse application/json
app.use(bodyParser.json())

let users = [
   {
      id:1,
      name: "Ali",
      age: 20
   },
   {
      id:2,
      name: "saja",
      age: 20
   },
   {
      id: 3,
      name: "ahmed",
      age: 19
   },
   {
      id: 4,
      name: "noor",
      age: 26
   },
   {
      id: 5,
      name: "mina",
      age: 30
   }
]

app.get('/', (req, res)=>{
   res.send('boo from root')
})

app.post('/', (req, res)=>{
   res.send('post')
})
// ام البيت تمشي ع كايد لاين ثابت مثل متسوي البامية ع دجاج :P

// app.get('/:age', (req, res)=>{
//    if (req.params.age > 20) {
//       res.status(202).send('accepted')
//    }

//    res.status(406).send('not accepted')
   
// })


app.get('/users', (req, res)=>{
   res.json(users)
})

app.post('/users', (req, res) => {
   var name = req.body.name
   if(name === '' || !name){
      res.status(400).send('name should not empty')
   }
   var new_user = {
      id: users.length + 1,
      name: req.body.name,
      age: req.body.age
   }
   users.push(new_user)
   res.send(users)
})

app.put('/users/:id', (req, res)=>{
   var user = _.find(users, (u)=>{
      return u.id == req.params.id
   })

   if(!user) res.status(204)
   user.name = req.body.name
   user.age = req.body.age

   res.send(user)
})

// TODO:
// app.delete('/users/:id', (req, res)=>{
//    var user = _.find(users, (u) => {
//       return u.id == req.params.id
//    })
//    if (!user) res.status(204)

//    var del = _.pullAllBy(users, user);

//    console.log(users);
   
//    res.send(users)
// })

app.get('/users/:age',(req, res)=>{
   let age = req.params.age
   let result = _.filter(users, function(user){
     return user.age == age 
   })

   if (result.length == 0) res.status(204)

   res.send(result)

})



app.listen(3000, ()=> {
   console.log('running on port 3000')
})