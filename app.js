// 10/1/2019

const express =  require('express')
var bodyParser = require('body-parser')
const app = express()
const _ = require('lodash')

app.use(bodyParser.urlencoded({
   extended: false
}))

// parse application/json
app.use(bodyParser.json())



let ar = [1,2,3,4,"A","B"]
let users = [
   {
      "name": "Ali",
      "age": 20
   },
   {
      "name": "Abdulla",
      "age": 21
   },
   {
      "name": "Noor",
      "age": 20
   },
   {
      "name": "Saja",
      "age": 23
   },
   {
      "name": "Ahmed",
      "age": 25
   },
]

app.use((req, res, next) => {
   console.log('this middleware')
   next()
})

app.get('/testing', (req, res)=>{

   // for(var i = 0 ; i < ar.length; i++){
   //    console.log(ar[i]);
   // }

   // ar.forEach(a => {
   //    console.log(a)
   // });

   // ar.map((a)=>{
   //    console.log(a)
   // })
   res.send(ar)
})




app.get('/users/:age', (req, res)=>{
   // var user = users.find((user)=> user.age == req.params.age);
   var data = _.filter(users, (user)=>{
      return user.age >= req.params.age
   })
   res.send(data)
})

app.post('/users', (req, res)=>{
   // console.log(req)
   res.send(req.body)
})


// RESTful API
// CRUD operations

// all users
app.get('/users', (req, res) => {
   res.send(users)
})
// get one user
app.get('/users/:name')
// add new user
app.post('/users')
// update user
app.put('users/:name')
// delete
app.delete('users/:name')

app.listen('5000', () => console.log('running on 5000'))